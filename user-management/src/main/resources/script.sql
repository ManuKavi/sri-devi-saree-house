DROP TABLE IF EXISTS sridevi.user_info;
DROP TABLE IF EXISTS sridevi.role;

CREATE TABLE sridevi.role(
 role_id INT PRIMARY KEY NOT NULL,
 role_name VARCHAR(50) NOT NULL
);

CREATE TABLE sridevi.user_info(
  user_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  password VARCHAR(15) NOT NULl,
  user_name VARCHAR(50) NOT NULL,
  mobile_number VARCHAR(10) NOT NULL,
  address VARCHAR(50) NOT NULL,
  role_id INT NOT NULL 
);

ALTER TABLE sridevi.user_info ADD FOREIGN KEY (role_id) REFERENCES sridevi.role(role_id);

INSERT INTO `sridevi`.`role` (`role_id`, `role_name`) VALUES ('1', 'Admin');
INSERT INTO `sridevi`.`role` (`role_id`, `role_name`) VALUES ('2', 'Super User');

INSERT INTO `sridevi`.`user_info` (`user_id`, `password`, `user_name`, `mobile_number`, `address`, `role_id`) VALUES ('1', '32323', 'Ramesh', '8585858585', 'Gadwal', '1');


