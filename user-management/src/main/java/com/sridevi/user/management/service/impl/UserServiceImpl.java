package com.sridevi.user.management.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sridevi.user.management.dao.UserRepository;
import com.sridevi.user.management.dto.UserDto;
import com.sridevi.user.management.entity.User;
import com.sridevi.user.management.service.UserService;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public List<UserDto> getUsers() {
		List<User>  user = userRepo.findAll();
		ModelMapper modelMapper = new ModelMapper();
		List<UserDto> userDto 
	    = modelMapper.map(user, new TypeToken<List<UserDto>>() {}.getType());
		return userDto;
	}

	@Override
	public UserDto getUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveUser(UserDto userDto) {
		ModelMapper modelMapper = new ModelMapper();
		User user = modelMapper.map(userDto, User.class);
		userRepo.save(user);
		return "User Created Successfully";
	}

	@Override
	public UserDto updateUser() {
		// TODO Auto-generated method stub
		return null;
	}

}
