package com.sridevi.user.management.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sridevi.user.management.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
}
