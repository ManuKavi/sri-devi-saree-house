package com.sridevi.user.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sridevi.user.management.dto.UserDto;
import com.sridevi.user.management.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@GetMapping("userlist")
	public List<UserDto> getUsers() {
		return userService.getUsers();
	}
	
	@PostMapping("create")
	public String createUser(@RequestBody UserDto userDto) {
		userService.saveUser(userDto);
		return "User Created Success fully";
		
	}

}
