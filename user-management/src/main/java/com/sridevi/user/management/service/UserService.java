package com.sridevi.user.management.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sridevi.user.management.dto.UserDto;


public interface UserService {
	
	public List<UserDto> getUsers();
	
	public UserDto getUser();
	
	public String saveUser(UserDto userDto);
	
	public UserDto updateUser();
	

}
